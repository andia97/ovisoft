// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://127.0.0.1:8000/api',
  // apiUrl: 'https://shielded-bastion-41106.herokuapp.com/api',
  firebase: {
    apiKey: "AIzaSyAljUh33Ud2h0DxASqM9F9CoRVomED1AGI",
    authDomain: "ovisoft-31213.firebaseapp.com",
    projectId: "ovisoft-31213",
    storageBucket: "ovisoft-31213.appspot.com",
    messagingSenderId: "361401249482",
    appId: "1:361401249482:web:1073b1d11a76d5bab7ce33",
    measurementId: "G-G0B8DM6V3L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
