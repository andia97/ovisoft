export interface CompanyDetail{
    id: number,
    company_id: number,
    description: string,
    image: string,
    create_at: string,
    update_at: string 
}