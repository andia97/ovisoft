export interface Company{
    id: number;
    title: string;
    description: string;
    video: string;
}