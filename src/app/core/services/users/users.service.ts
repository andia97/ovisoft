import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../model/user.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpService: HttpService) { }

  getAllusers(): Observable<any> {
    return this.httpService.get('/usuario');
  }

  getAllroles(): Observable<any> {
    return this.httpService.get('/rol');
  }

  getUser(id: number): Observable<any> {
    return this.httpService.get('/usuario/'+id);
  }

  addUser(dataForm: User): Observable<any> {
    return this.httpService.post('/register', dataForm);
  }

  editUser(dataForm: User, id: number): Observable<any> {
    return this.httpService.put('/usuario/' + id, dataForm);
  }

  deleteUser(id: number): Observable<any> {
    return this.httpService.delete('/usuario/' + id);
  }
}
