import { AuthConstants } from './../../config/auth-constanst';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{
  token: any;
  constructor(
    private storageService : StorageService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.storageService.get(AuthConstants.AUTH).then(
      (res:any)=>{
        this.token = res;
      }
    )
    .catch(
      (res:any)=>{
        this.token = null || undefined;
      }
    )
    const token: any = localStorage.getItem('authToken');
    let request = req;

    if (this.token) {
      request = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        }
      });
    }

    return next.handle(request);
  }
}
