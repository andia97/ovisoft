import { Injectable} from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, retry } from 'rxjs/operators';
import { MessageService } from './message.service';

@Injectable({providedIn: 'root'})
export class InterceptorMessage implements HttpInterceptor{

  constructor(
    private serviceMessage: MessageService,
    private router: Router,
    ){
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
      
      return next.handle(req)
      .pipe( 
          retry(1),
          catchError((error: HttpErrorResponse) =>{
            //   console.log(error)
              let errormen = error.error;
            //   console.log(error.error)
              // this.serviceMessage.add(error.error.message)
              this.serviceMessage.add(error.error)
              if( error.error instanceof ErrorEvent){
                  switch(error.status){
                      case 422:
                          errormen = error.error.message;
                          console.log(error.error)
                          // this.serviceMessage.add(error.error.message)
                          this.serviceMessage.add(error.error.message);
                      break
                  }
              }else{
                  switch(error.status){
                      case 422:
                          this.serviceMessage.add(error.error.errors.name[0]);
                        //   console.log(error.error)
                          // this.serviceMessage.add(error.error.message);
                      break
                      case 401:
                          // this.serviceMessage.add(error.error.errors.name[0]);
                        //   console.log('No tiene acceso permitido');
                          this.router.navigate(['login']);
                          this.serviceMessage.add('');
                      break
                      case 0:
                          this.serviceMessage.add(error.error);
                      break
                  }
                  
              }
              errormen = error.error.message;
              console.log();
              return throwError(errormen)
          })
      ); 
  }
}
