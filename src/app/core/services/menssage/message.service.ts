import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MessageService {

  message: any;

  constructor() { }

  add(message: any){
    this.message  = message ; 
  }

  clear(){
    this.message = "";
  }

  get(){
    return this.message;
  }
}
