import { AuthConstants } from './../../config/auth-constanst';
import { StorageService } from './../storage/storage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../model/user.model';
import { HttpService } from '../http/http.service';
import { AuthUser } from '../../model/auth.model';

import { Response } from '../../model/response.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router,
    
  ) {  }

  login(data: AuthUser): Observable<any>{
    return this.httpService.post('/login', data)
    .pipe(
      catchError(
        this.handleError
      ),
      map(
        (res: Response) => {
          
          const token: string = res.data.token;
          const dataUser: User = res.data.user;
          const rol:string = res.data.user.role_id;
          
          this.storageService.store(AuthConstants.TITLE_NAV, 'Home');
          this.storageService.store(AuthConstants.AUTH, token);
          this.storageService.store(AuthConstants.USER, dataUser);
          this.storageService.store(AuthConstants.ROL, rol);


          this.router.navigate(['ovisoft']);
          return "Bienvenido";
        },
      )
    );
  }
  
  logout(data: string): Observable<any>{
    return this.httpService.post('/logout', data)
    .pipe(
      catchError(
        this.handleError
      ),
      map(
        (res: Response) => {
          this.storageService.clear();
          this.router.navigate(['home']);
          window.location.reload();
          return "Se cerró la sesión correctamente";
        },
      )
    );
    // this.router.navigate(['login']);
  }

  private handleError(httpResponse: HttpErrorResponse) {
    const error = httpResponse.error;
     return throwError(error);
  }
}
