import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { Response } from '../../model/response.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpService: HttpService) { }

  public getHome(): Observable<Response[]>{
    return this.httpService.get('/home')
    .pipe(
      map(
        (res: Response[]) => {
          //const homeData: [] = res.data;
          return res;
        }
      )
    )
  }

  public updateHome(id: number, data:any){
    return this.httpService.put(`/home/${id}`, data)
  }
}