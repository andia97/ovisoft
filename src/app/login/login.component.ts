import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../core/services/login/login.service';
import { AuthUser } from '../core/model/auth.model';
import { StorageService } from '../core/services/storage/storage.service';
import { AuthConstants } from '../core/config/auth-constanst';
import { User } from '../core/model/user.model';

import Swal from 'sweetalert2';
import { ErrorsMessage } from '../core/model/errors.model';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide: boolean = true;
  loginForm!: FormGroup;
  state: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private storageService: StorageService
  ) {
    this.buildForm();
  }
  ngOnInit(): void {
  }

  get emailUser(): any {
    return this.loginForm.get('email');
  }

  get passwordUser(): any {
    return this.loginForm.get('password');
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', { validators: [Validators.required, Validators.minLength(4), Validators.maxLength(30), Validators.email] }],
      password: ['', { validators: [Validators.required, Validators.minLength(4), Validators.maxLength(15)] }]
    });
  }

  login() {
    this.state = true;
    const dataLoginForm: AuthUser = this.loginForm.value;
    this.loginService.login(dataLoginForm)
    .subscribe(
      (res: string) => {
        this.state = false;
        Swal.fire({
          icon: 'success',
          title: res,
          text: 'Ya puedes editar la información de tu página',
          timer: 2000,
          showConfirmButton: false   
        })
      },(error: ErrorsMessage)=>
      {
        this.state = false;
        Swal.fire({
          icon: 'error',
          title: 'Error de autentificacion',
          text: 'El email/contraseña son incorrectos',
          timer: 3000,
        })
      })
  }
}
