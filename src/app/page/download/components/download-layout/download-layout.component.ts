import { Component, OnInit } from '@angular/core';
import { File } from 'src/app/core/model/download.model';
import { DocumentService } from 'src/app/core/services/document/document.service';
@Component({
  selector: 'app-download-layout',
  templateUrl: './download-layout.component.html',
  styleUrls: ['./download-layout.component.scss']
})
export class DownloadLayoutComponent implements OnInit {
  files!: File[];
  state: boolean = false;
  constructor(
    private documentService: DocumentService
  ) { }

  ngOnInit(): void {
    this.getDocuments();
  }

  getDocuments(): void {
    this.state = true;
    this.documentService.getDocuments().subscribe(
      (res: File[]) => {
        this.state = false;
        // console.log(res);
        this.files = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  addDocument(): void {
    const document: File | any = {
      title: 'Nuevo Documento',
      subtitle: 'Subtitulo',
      image: 'https://st.depositphotos.com/2704315/3612/v/600/depositphotos_36124369-stock-illustration-vector-folder-with-documents-icon.jpg',
      url: 'https://firebasestorage.googleapis.com/v0/b/ovisoft-31213.appspot.com/o/documents%2Fdefautl.pdf?alt=media&token=7489627f-40da-47d3-8d7f-b9c1a9384160',
      description: 'Descripción',
      date: new Date,
    };
    this.documentService.addDocument(document)
    .then(
      (res: any) => {
        console.log(res);
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }
}
