import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutLayoutComponent } from './components/about-layout/about-layout.component';
import { AboutDetailComponent } from './components/about-detail/about-detail.component';
import { AngularMaterialModule } from 'src/app/shared/agular-material/angular-material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ContainerAboutComponent } from './container/container-about/container-about.container';

@NgModule({
  declarations: [
    AboutLayoutComponent,
    AboutDetailComponent,
    ContainerAboutComponent
  ],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class AboutUsModule { }
