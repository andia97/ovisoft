import { StorageService } from './../../../../core/services/storage/storage.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AboutUsService } from 'src/app/core/services/about-us/about-us.service';
import { Company } from 'src/app/core/model/company.model';
import { MatDialog } from '@angular/material/dialog';
import { AboutDetailComponent } from '../../components/about-detail/about-detail.component';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
@Component({
  selector: 'app-container-about',
  templateUrl: './container-about.container.html',
  styleUrls: ['./container-about.container.scss']
})
export class ContainerAboutComponent implements OnInit {
  aboutUsForm!: FormGroup;
  rol!: Number;
  

  isTitle: boolean=false;
  isDescription: boolean = false;
  
  @Input() item!: Company;
  @Input() index!: Number;

  @Output() evenEmiiter: EventEmitter<any> = new EventEmitter();
  constructor(
    
    private formBuilder : FormBuilder,
    private aboutUsService: AboutUsService,
    public dialog: MatDialog,
    private storageService: StorageService

  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.aboutUsForm.patchValue(this.item);
    this.isAdminUser();
  }
  
  openDialogAboutUsDetail():void
  {
    const dialogRef = this.dialog.open(AboutDetailComponent,
      { 
        maxWidth: '100vw',
        maxHeight: '100vh',
        width:'100%',
        height: '100%',
      });
  }

  
  private buildForm(){
    this.aboutUsForm = this.formBuilder.group({
      title: [''],
      description: ['']
    });
  }

  
  setIsHiden(option: string): void{
    switch (option) {
      case 'title':
        if (!this.isTitle && this.rol===1) {
          this.isTitle = true;
        } else {
          this.isTitle = false;
        }
      break;
      case 'description':
        if (!this.isDescription  && this.rol===1) {
          this.isDescription = true;
        } else {
          this.isDescription = false;
        }
      break;
    }
  }
  
  updateAboutUs(option: string): void{
    switch (option) {
      case 'title':
        this.isTitle = false;
      break;
      case 'description':
        this.isDescription = false;
      break;
    }
    this.aboutUsService.updateAboutUS(this.item.id, this.aboutUsForm.value)
    .subscribe(
      (res: Company)=>
      {
        this.evenEmiiter.emit(true);
      }
    )
  }

  isAdminUser(){
    this.storageService.get(AuthConstants.ROL).then(
      (res:any)=>{
        this.rol = res;
        console.log(this.rol);
        
      }
    )
  }
}
