import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit , Inject } from '@angular/core';
//import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AboutUsService } from 'src/app/core/services/about-us/about-us.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
@Component({
  selector: 'app-about-detail',
  templateUrl: './about-detail.component.html',
  styleUrls: ['./about-detail.component.scss']
})
export class AboutDetailComponent implements OnInit {
  urlImg = 'https://img.freepik.com/vector-gratis/fondo-numeros-datos-digitales-codigo-binario_1017-30367.jpg?size=626&ext=jpg';
  isDescription: boolean = false;
  isImage: boolean = false;
  isTitle: boolean = false;
  aboutUsDetailData: any;
  formAboutDetail!: FormGroup;
  rol!: Number;

  constructor(
    private aboutUsService: AboutUsService,
    private formBuilder: FormBuilder,
    private storageService: StorageService
  ) { 
    this.buildForm();
  }
    /*
    closeDialogAboutUsDetailComponent(): void {
      this.dialogRef.close();
    }*/

  ngOnInit(): void {
    this.getAboutUsDetail();
    this.isAdminUser();
  }
  private buildForm(){
    this.formAboutDetail = this.formBuilder.group({
      description: [''],
      image: ['']
    })
  }
  setIsHiden(option: string): void {

    switch (option) {
      case 'description':
        if (!this.isDescription && this.rol === 1) {
          this.isDescription = true;
        } else {
          this.isDescription = false;
        }
      break;
      case 'image':
        if (this.isImage) {
          this.isImage = false;
        } else {
          this.isImage = true;
        }
      break;
    }
  }

  isAdminUser(){
    this.storageService.get(AuthConstants.ROL).then(
      (res:any)=>{
        this.rol = res;
      }
    )
  }

  getAboutUsDetail(){
    this.aboutUsService.getAboutUSDetail(1)
    .subscribe(
      (res:any)=>{
        this.aboutUsDetailData = res.data;
        this.formAboutDetail.patchValue(this.aboutUsDetailData);
      }
    )
  }

  editAboutUsDetail(option: string){
    switch (option) {
      case 'description':
        this.isDescription = false;
      break;
      case 'image':
        this.isImage = false;
      break;
    }
    this.aboutUsService.updateAboutDetail(this.aboutUsDetailData.id ,this.formAboutDetail.value)
    .subscribe(
      (res:any)=>{
        this.getAboutUsDetail();
      }
    )
  }
}
